# Nextflow ![image](../img/nextflow2014.png)
---
En amont du hackathon vous pouvez si vous le souhaitez suivre les turoriels suivants.

## @Ceux qui n'ont jamais lancé de workflow nextflow 
Voici un tutoriel pour apprendre a lancer des workflows nextflow, comprendre l
'utilisation de singularity au sein de nextflow et enfin lancer des 
workflows nfcore. Durant le hackathon nous consacrerons du temps pour 
les questions réponses lié a ce tutoriel.
https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/


## @Ceux qui souhaitent developper avec nextflow
En amont du hackathon vous pouvez si vous le souhaitez faire le tutoriel 
disponible a cette adresse, en sachant que c'est le tutoriel que l'on suivra 
lors du hackathon.

* Se connecter sur genotoul via un terminal ou via putty :
  ```
  ssh -X USERNAME@genologin.toulouse.inra.fr
  ```
* Charger le module nextflow et singularity:
  ```
  module load bioinfo/Nextflow-v20.01.0
  module load system/singularity-3.5.3
  ```
* Créer l'environnement adapté à genologin:
  ```
  sh /usr/local/bioinfo/src/NextflowWorkflows/create_nfx_dirs.sh
  ```

* Aller dans le work (espace de travail accessible depuis les noeuds en lecture/écriture)
  ```
  cd ~/work
  ```

* Récupérer le matériel pour le TP: 
  ```
  git clone https://github.com/seqeralabs/nextflow-tutorial.git && cd nextflow-tutorial
  ``` 

* Suivre les instructions à partir de la partie 2: https://seqera.io/training/

Voici des liens vers les vidéos du hackathon nextflow de juillet 2020 du CRG. 
En suivant le tuto vous pourez voir les tranches de temps correspondant aux différents points abordés.

* Day 1 : https://www.youtube.com/watch?v=8_i8Tn335X0 point important pointé ci-dessous :
  * Concept de base 20mn-28mn
  * Portabilité & Conteneurisation: 33m-38m
  * Démarrage du tutoriel 1h08m30-1h36m
     * 1h08m30 Introduction
     * 1h11m40 Explication du premier `hello.nf`
     * 1h27m00 -resume
     * 1h29m00 --params 
     * 1h31m45 operator flatten() 
* Day 2 : https://www.youtube.com/watch?v=j5Xc8mUmDMc : commencer à 4m22
  * Channel 4m22
  * Channel factories 6m30
  * Channels
    * 10:36 Value channels
    * 11:35 Channels factories
    * 16:27 Channel.from 
      * 17:30 Closure input_ch.view{ "value: it" }
    * 19:00 Channel.of
    * 21:20 Channel.fromPath
    * 27:40 Channel.fromFilePairs
    * 39:40: Channel.fromSRA
    * 45:30: Utilisation des channel dans un process et des variables associées
  * Processes 49:00
    * 56:00 Channel utilisable dans code python ?
    * 57:00 Script parameters
    * 1:02:00 Conditional Script
    * 01:04:24 4.2-Inputs
    * 01:07:46 Inputs files 
      _* 01:10:00 erreur fichier input.1 est créé sans que je l'ai demandé_
      _* 01:18:31 path (convertis automatiquement un string en file channel )_
    * 01:21:15 Combine input channels
    * 01:32:00 Input repeaters
    * 01:36:30 Outputs
    * 01:45:45 When

* Day 3 : https://www.youtube.com/watch?v=xmNUtboTThA
   * 04:42 directives 
      * 11:00 directive publishDir
      * 16:45 manage semantic sub-directories
   * 20:00 operator 
      * view (un peu long, closure expliquée en détail)
      * 29:00 map (un peu long aussi)
      * 37:30 into (pour cloner des channels !) & mix
      * 39:10 flatten 
      * 41:00 collect 
      * 42:15 groupTuple (top!)
      * 46:55 join
      * 49:00 branch (sexy!)
   * 50:30 Groovy
   * 57:00 Simple Rnaseq pipeline
      * 01:06 : container in nextflow (sur genotoul utiliser -with-singularity)
    
* Day 4 : https://www.youtube.com/watch?v=XFrVtD-RPzY
  * Demarre avec Docker -> pas adapté.
  * 42min40 -> 53min Singularity
  * 56min: conda
  * 1h05: configuration

* Day 5 : https://www.youtube.com/watch?v=IcMz6JE8n_M

