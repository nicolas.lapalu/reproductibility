# Summary

* [Introduction](README.md)
* [Singularity](singularity/README.md)
* [Snakemake](snakemake/README.md)
* [Nextflow](nextflow/README.md)