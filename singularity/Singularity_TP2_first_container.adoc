[.left]
image::../../img/singularity.png[]

= How to build a Singularity container

:icons: font

<<<
Based on : https://gitlab.in2p3.fr/alexandre.dehne-garcia/TP_singularity_EcoleConteneursProd.git
 (Alexandre Dehne Garcia, Rémy Dernat, Martin Souchal)

All the files mentioned in this recipe are available in the following git repository :

[source, bash]
git clone https://forgemia.inra.fr/inter_cati_omics/reproductibility.git

This recipe describes how to build an Ubuntu image using Singularity, with the Singularity 3 version (3.5.2). You can download Singularity and find some docs here: https://www.sylabs.io/docs/

We are going to build containers and run them on an HPC cluster genotoul. footnote:[We are grateful to the genotoul bioinformatics platform Toulouse Occitanie (Bioinfo Genotoul, doi: 10.15454/1.5572369328961167E12) for providing help and/or computing and/or storage resources]

HELP:

* https://sylabs.io/guides/3.5/user-guide/
* https://groups.google.com/a/lbl.gov/forum/#!forum/singularity
* https://singularity.lbl.gov/docs-recipes#best-practices-for-build-recipes

==== Sources are in the folder TP2
=== Use singularity on GenoToul bioinformatics facility: 

Connection with ssh and work in your work folder:
[source, bash]
ssh <mylogin>@genologin.toulouse.inra.fr

On GenoToul Singularity is available using module:
[source, bash]
module load system/singularity-3.5.3

== 1) Use pre-built Singularity images

You can use the pull and build commands to download pre-built images from an external resource like Singularity Hub or Docker Hub. When called on native Singularity images like those provided on Singularity Hub, pull simply downloads the image file to your system.

==== singularity pull
Pull an image from a URI

Synopsis

The ‘pull’ command allows you to download or build a container from a given URI. 

Supported URIs include:

----
library: Pull an image from the currently configured library
library://user/collection/container[:tag]

docker: Pull an image from Docker Hub
docker://user/image:tag

shub: Pull an image from Singularity Hub
shub://user/image:tag

oras: Pull a SIF image from a supporting OCI registry
oras://registry/namespace/image:tag

http, https: Pull an image using the http(s?) protocol
https://library.sylabs.io/v1/imagefile/library/default/alpine:latest
----

On Genotoul, use your "work" folder: /work/<mylogin>

[source, bash]
cd /home/<mylogin>/work
singularity pull shub://GodloveD/lolcow

Then run it :
[source, bash]
singularity run lolcow_latest.sif

Or in one line :
[source, bash]
singularity run shub://GodloveD/lolcow

You can also use pull with the docker:// URI to reference Docker images served from a registry. In this case, pull does not just download an image file. Docker images are stored in layers, so pull must also combine those layers into a usable Singularity file.

To run it using SLURM on Genotoul:
[source, bash]
----
#!/bin/sh
#SBATCH --job-name=lolcow
#SBATCH -N 1
#SBATCH --ntasks-per-node=1
#SBATCH --time=01:00:00

module load system/singularity-3.5.3

echo « Running on: $SLURM_NODELIST »
singularity run lolcow_latest.sif

----



[source, bash]
singularity pull docker://godlovedc/lolcow  # with the default name

[source, bash]
singularity pull --name funny.sif docker://godlovedc/lolcow # with custom name

NOTE: With this method, you have one compressed read-only squashfs file system suitable for production, but you can also use writable ext4 file system suitable for interactive development (in order to build the image you have to be root):
----
  Build a base sandbox from DockerHub, make changes to it, then build sif
    sudo singularity build --sandbox debian docker://debian:latest
    sudo singularity exec --writable debian apt-get update
    sudo singularity exec --writable debian apt-get install python
    sudo singularity build debian2.sif debian
----

== 2) Build images from scratch

A Singularity Recipe is the driver of a custom build, and the starting point for designing any custom container. It includes specifics about installation software, environment variables, files to add, and container metadata. You can even write a help section, or define modular components.

In the Docker world, containers recipe are called Dockerfile. With Singularity you can use Dockerfile or Singularity Recipe. You can also import already built Docker container.

The header is at the top of the file, and tells Singularity the base Operating System that it should use to build the container. It is composed of several keywords. Specifically:

* Bootstrap: references the kind of base you want to use (e.g., docker, debootstrap, shub). For example, a shub bootstrap will pull containers for shub as bases. A Docker bootstrap will pull docker layers to start your image. For a full list see build
* From: is the named container (shub) or reference to layers (Docker) that you want to use (e.g., vsoch/hello-world)

Depending on the value assigned to Bootstrap, other keywords may also be valid in the header.

Create a text file with the recipe above. Call it Steam_Locomotive.def (available in TP2).
[source,python]
----
BootStrap: docker
From: ubuntu:16.04

%help
My first train container
Run the sl Steam Locomotive

%setup
mkdir ${SINGULARITY_ROOTFS}/data

%post
apt-get -y update
#install sl :"Steam Locomotive" package
apt -y install sl

%help
Help me. The train is stuck in this container.

%labels
Maintainer author1
Updater author2
ContainerVersion v1.0
Software "Steam Locomotive" package: sl
Default runscript: sl

%environment
    export LC_ALL=en_US.utf8
    export PATH=/usr/games:$PATH

%runscript
sl
echo passed arguments are "$*"

----


Build it (reminder: you have to be root!):
[source, bash]
sudo singularity build Steam_Locomotive.sif Steam_Locomotive.def

Then run it :
[source, bash]
singularity run Steam_Locomotive.sif arg1 bb arg3
./Steam_Locomotive.sif a1 BB a3

Once you have an image, you can interact with it in several ways.

The shell command allows you to spawn a new shell within your container and interact with it as though it were a small virtual machine :
[source, bash]
singularity shell Steam_Locomotive.sif
which sl
exit

You can start the train in the container, or just run commands :
[source, bash]
singularity exec Steam_Locomotive.sif /usr/games/sl
singularity exec Steam_Locomotive.sif echo toto
singularity exec Steam_Locomotive.sif cat /.singularity.d/labels.json

If you're lost, you can call for help :
[source, bash]
singularity run-help Steam_Locomotive.sif

You can also bind host directory inside your container :
[source, bash]
singularity shell --bind /usr:/mnt/usrHost Steam_Locomotive.sif


With help from the official doc : https://sylabs.io/guides/3.5/user-guide/definition_files.html, edit Steam_Locomotive recipe to import a soulTrain.txt file inside your container, and display it after the train (without using bing option).

Before rebuilding your container, delete your first image. Then use the command `build`.

soulTrain.txt :
----
Best Soul Train songs are :
1. Marvin Gaye, "Got To Give It Up"
2. Earth, Wind & Fire, "Let's Groove"
3. The O'Jays, "I Love Music"
4. Slave, "Just A Touch of Love"
5. The Spinners, "It's A Shame"
----

=== Interact with containers

Once you have an image, you can interact with it in several ways. Let's try to test some interesting commands :


===== same network in or out the container

To check, list host network interfaces :
[source, bash]
sudo ifconfig

Then in a container :
[source, bash]
sudo singularity shell Steam_Locomotive.sif --writable /tmp/debian
apt install net-tools
ifconfig
exit

What can you conclude?


===== Defaults bind

By default singularity bind mounts /home/$USER, /tmp, and $PWD into your container at runtime. You can specify additional directories to bind mount into your container with the - -bind option. In this example, the data directory on the host system is bind mounted to the /mnt directory inside the container.

Let's check :
[source, bash]
ls /home/
touch $HOME/proofHOME
touch /tmp/proofTMP
touch /var/tmp/proofVAR_TMP
cat /etc/resolv.conf
cat /etc/hosts
singularity shell docker://ubuntu
ls /home/
ls -l $HOME/proofHOME  /tmp/proofTMP /var/tmp/proofVAR_TMP
cat /etc/resolv.conf
cat /etc/hosts
exit

By default, a user cannot access others home directory.

You can try these too :
[source, bash]
singularity shell docker://ubuntu
mount
ls -l
exit

===== userspace

With Singularity, you are the same user outside and inside the container, it's why to be root in your container you have to use sudo command.

Let's check :
[source, bash]
whoami
id
singularity shell docker://ubuntu
whoami
id
exit

Singularity use user namespace.

===== same PID inside and outside the container

The container is similar to logging into another identical host running a different operating system. e.g. One moment you are on Centos-6 and the next minute you are on the latest version of Ubuntu that has Tensorflow installed, or Debian with the latest OpenFoam, or a custom workflow that you installed. But you are still the same user with the same files running the same PIDs. Additionally, the selection of name-space virtualization can be dynamic or conditional. For example, the PID namespace is not separated from the host by default, but if you want to separate it, you can with a command line (or environment variable) setting.

[source, bash]
ps
# note the host PID
singularity shell docker://ubuntu
ps
# you can find same PID inside
exit

If you open a shell in your container, you will see every process running on the host. If you want to run your container in a different namespace, you can use -p option.

Try to run your container with and without -p and enter top command.

====== Environment Variables

If you need to define a variable at runtime, set variables inside the container by prefixing them with SINGULARITYENV_. They will be transposed automatically and the prefix will be stripped :

[source, bash]
SINGULARITYENV_FOO=foo singularity shell docker://ubuntu
echo $FOO
exit

If you build a container from Singularity Hub or Docker Hub, the environment will be included with the container at build time. You can also define custom environment variables in your Recipe file as follows:

[source, bash]
%environment
    VARIABLE_NAME=VARIABLE_VALUE
    export VARIABLE_NAME

Some predefined environment variables are useful, like SINGULARITY_ROOTFS which refers to the root (/) of the container.


==== TIP : Sandbox container

If you wanted to create a container within a writable directory (called a sandbox) you could do so with the --sandbox option.

Let's try to build our container with the minimal recipe, in sandbox mode to be able to add stuff later :

_min.def_ :
----
BootStrap: docker
From: ubuntu:16.04
----

[source, bash]
sudo singularity build --sandbox min.sandbox min.def

The resulting directory operates just like a container in an image file. You are permitted to make changes and write files within the directory, but those changes will not persist when you are finished using the container. To make your changes persistent, use the --writable flag when you invoke your container.

Let's try to add wget command in our container :

[source, bash]
sudo singularity shell --writable min.sandbox
apt-get update
apt-get install wget build-essential

To exit the container, just type "exit".

Now, to build a scashfs image do:
[source, bash]
sudo singularity build min.sif min.sandbox




<<<
