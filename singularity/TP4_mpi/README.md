# openmpi Singularity container with mpitest

create a folder with these 3 files:<br>
.gitlab-ci.yml<br>
mpitest.c<br>
openmpi.def<br>

run:<br>

`git init`<br>
`git remote rm origin`<br>
`git remote add origin git@forgemia.inra.fr:<yourgitlabaccount>/openmpi.git`<br>
`git add .`<br>
`git commit -m "Initial commit"`<br>
`git remote -v`<br>
`git push -u origin master`<br>


Singularity container based on the recipe: openmpi.def<br>

the build compile the mpitest.c and create the image openmpi.sif <br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
`singularity pull openmpi.sif oras://registry.forgemia.inra.fr/inter_CATI_omics/openmpi/openmpi:latest`


