# ![image](../img/snakemake_logo.png)  Snakemake
---

## @Ceux qui n'ont jamais lancé de workflow snakemake
Voici un tutoriel pour apprendre à écrire et lancer un workflow basique en snakemake, 
comprendre l'utilisation de conda au sein de snakemake et enfin lancer des workflows snakemake.
* https://snakemake.readthedocs.io/en/stable/tutorial/short.html  
  * démo officielle: https://youtu.be/hPrXcUUp70Y   

Je recommande de suivre la vidéo tuto, présenté par Johannes Köster en personne, 
avec le tuto en ligne en support, car elle expose et reprend les mêmes ressources.  

Retrouver les étapes et solutions au short tutoriel snakemake/conda sur genotoul [ici](https://inter_cati_omics.pages.mia.inra.fr/hackathon_snakemake_2020/short_tutorial/#short-tutorial-avec-conda).  
 
todo: version à venir
* adapter le tuto avec une version genotoul + singularity

## @Ceux qui souhaitent développer leur propre workflow snakemake
Voici une série de liens vers des tutoriels avancés, que vous pouvez 
travailler en amont du hackathon:
* [tutoriel officiel](https://snakemake.readthedocs.io/en/stable/tutorial/tutorial.html)  
    Tout ce qu'il faut savoir et ossible de faire avec snakemake
* [introduction à snakemake par la fondation carpentry](https://hpc-carpentry.github.io/hpc-python/11-snakemake-intro/)
    Une introduction aux principales fonctionnalités de snakemake, moins bioinfo, mais tout à fait compréhensible.  
    Avec une très bonne intro générale aux [pipelines d'analyse en python](https://hpc-carpentry.github.io/hpc-python/)  
* [introduction à snakemake par Titus Brown](https://hackmd.io/7k6JKE07Q4aCgyNmKQJ8Iw?view)
    une très bonne intro avec un tuto pour découvrir snakemake en 3h

Durant le hackathon, nous consacrerons du temps pour les questions 
réponses liés à ces tutoriels. L'idée n'est pas de faire tous les tutoriels, quoique :),
mais plutôt de vous donner des exemples concrets sur les fonctionnalités et possibilités
qu'offrent snakemake pour construire votre propre workflow.  
  
## Plus de ressources snakemake  
  
Ici vous trouverez des ressources additionnels pour découvrir et aussi en apprendre davantage sur snakemake, au travers de cas d'utilisation par exemple:  
* [application de snakemake sur la plateforme EPITRANS](https://pepi-ibis.inrae.fr/sites/default/files/AG2019/06-Use%20of%20Snakemake%20on%20EPITRANS%20platform%20_%20IPS2%20Paris-Saclay_jtran_20190606.pdf)  
* [tuto snakemake hackathon 2019](https://gitlab.in2p3.fr/inra_hackathon_inter_cati/snakemake)  
* [Sustainable data analysis with Snakemake](https://zenodo.org/record/4067137#.X5puQ3VKikA)  
* [workflows snakemake, exemples de bonnes pratiques](https://github.com/snakemake-workflows/docs)  
* [wrappers, registre de modules snakemake prêt à l'emploi](https://snakemake-wrappers.readthedocs.io/en/stable/)    
